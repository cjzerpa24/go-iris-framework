package resources

import (
	"strconv"
	"time"
)

type resources interface {
	DateNow() int
}

func DateNow() int {
	now := time.Now()
	hoy, _ := strconv.Atoi(now.Format("20060102"))
	return hoy
}
