package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"
	"github.com/kataras/iris"
	"go_iris/models"
	"go_iris/routes"
	"strconv"
)

var (
	server   = "localhost"
	port     = 1433
	user     = "sa"
	password = "987654321Sa*"
	database = "DB_Base"
)


func main() {

	dbuser := user
	dbpassword := password
	dbhost := server
	dbport := port
	dbname := database

	db, err := gorm.Open("mssql", "sqlserver://"+dbuser+":"+dbpassword+"@"+dbhost+":"+strconv.Itoa(dbport)+"?database="+dbname)
	// End the program with an error if it could not connect to the database
	// End the program with an error if it could not connect to the database
	if err != nil {
		panic("could not connect to database")
	}
	fmt.Printf("Connected!\n")

	// Create the default database schema and a table for the orders
	db.LogMode(true)
	db.AutoMigrate(&models.CLI_Cliente{})
	db.AutoMigrate(&models.FAC_Facturas{})
	db.AutoMigrate(&models.DFA_Detalles{})
	/*,&models.FAC_Facturas{},&models.DFA_Detalles{})
	*/
	// Closes the database connection when the program ends
	defer func() {
		_ = db.Close()
	}()

	app := iris.New()
	app.Logger().SetLevel("debug")

	//static files
	app.StaticWeb("/", "./public")
	app.RegisterView(iris.HTML("./templates", ".html"))


	routes.Router(db,app)

	//error
	app.OnAnyErrorCode(func(ctx iris.Context) {
		ctx.ViewData("Message", ctx.Values().GetStringDefault("message", "The page you're looking for doesn't exist"))
		ctx.View("error.html")
	})

	//start
	app.Run(
		iris.Addr(":9000"),
		iris.WithoutServerError(iris.ErrServerClosed),
		iris.WithOptimizations,
	)


}


