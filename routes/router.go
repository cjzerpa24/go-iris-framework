package routes

import (
	"github.com/jinzhu/gorm"
	"github.com/kataras/iris"
	"go_iris/handlers"
	"go_iris/repos"
	"go_iris/services"
)

const version = "1.0"

func Router(db *gorm.DB, app *iris.Application) {
	clienteRepo := repos.NewClienteRepository(db)
	clienteService := services.NewClientesService(clienteRepo)
	facturaRepo := repos.NewFacturaRepository(db)
	facturaService := services.NewFacturasService(facturaRepo)
	detalleRepo := repos.NewDetalleRepository(db)
	detalleService := services.NewDetallesService(detalleRepo)

	app.Use(func(ctx iris.Context) {
		ctx.Header("Server", "Iris API/"+version)
		ctx.Next()
	})

	goAPI := app.Party("/api/v1")
	{
		/* Cliente Handler */
		clienteHandler := handlers.NewClienteHandler(clienteService)
		goAPI.Get("/clientes", clienteHandler.GetAll)
		goAPI.Get("/clientes/id/:id", clienteHandler.GetById)
		goAPI.Get("/clientes/codigo/:codigo",clienteHandler.GetByCodigo)
		goAPI.Get("/clientes/nombre/:nombre",clienteHandler.GetByNombre)
		goAPI.Get("/clientes/nombre/:nombre/codigo/:codigo",clienteHandler.GetByCodigoNombre)
		goAPI.Post("/clientes", clienteHandler.AddCliente)
		goAPI.Put("/clientes", clienteHandler.UpdateCliente)
		goAPI.Delete("/clientes", clienteHandler.UpdateStatusCliente)

		/* Factura Handler */
		facturaHandler := handlers.NewFacturaHandler(facturaService)
		goAPI.Get("/facturas", facturaHandler.GetAll)
		goAPI.Get("/facturas/id/:id", facturaHandler.GetById)
		goAPI.Get("/facturas/codigo/:factura",facturaHandler.GetByCodigoFactura)
		goAPI.Get("/facturas/clientes/codigo/:codigo",facturaHandler.GetByCodigoCliente)
		goAPI.Get("/facturas/clientes/nombre/:nombre",facturaHandler.GetByNombreCliente)
		goAPI.Get("/facturas/concepto/:concepto",facturaHandler.GetByConcepto)
		goAPI.Post("/facturas", facturaHandler.AddFactura)
		goAPI.Put("/facturas", facturaHandler.UpdateFactura)
		goAPI.Delete("/facturas", facturaHandler.DeleteFactura)

		/* Detalle Handler */
		detalleHandler := handlers.NewDetalleHandler(detalleService)
		goAPI.Get("/facturas/id/:id/item", detalleHandler.GetAll)
		goAPI.Get("/facturas/id/:id/item/:id_detalle", detalleHandler.GetById)
		goAPI.Post("/facturas/id/:id/item", detalleHandler.AddDetalle)
		goAPI.Put("/facturas/id/:idfactura/item/id/:iditem", detalleHandler.UpdateDetalle)
		goAPI.Delete("/facturas/id/:id/item", detalleHandler.DeleteDetalle)
	}




/*
	//Get By Fecha Desde y Fecha Hasta
	app.Handle("GET","/facturas/desde/:desde/hasta/:hasta", func(ctx iris.Context) {
		fechaDesde := ctx.Params().Get("desde")
		fechaHasta := ctx.Params().Get("hasta")
		fac, err := facturaService.GetByFechas(fechaDesde,fechaHasta)
		if err != nil {
			ctx.StatusCode(404)
			ctx.JSON(context.Map{"response": "No existen registros"})
		} else {
			ctx.StatusCode(200)
			ctx.JSON(context.Map{"response": fac})
		}
	})*/


}
