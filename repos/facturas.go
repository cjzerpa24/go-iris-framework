package repos

import (
	"github.com/jinzhu/gorm"
	"go_iris/models"
	"strconv"
	"strings"
	"sync"
	"time"
)

type FacturaRepository interface {
	Select() ([]models.FAC_Facturas,error)
	SelectById(id int) (models.FAC_Facturas,error)
	SelectByNro(nro_factura string) ([]models.FAC_Facturas,error)
	SelectByCodigoCli(codigoCli string) ([]models.FAC_Facturas,error)
	SelectByNombreCli(nombreCli string) ([]models.FAC_Facturas,error)
	SelectByFechas(fechaIni string, fechaFin string) ([]models.FAC_Facturas,error)
	SelectByConcepto(concepto string) ([]models.FAC_Facturas,error)
	Insert(factura models.FAC_Facturas) (result models.FAC_Facturas, err error)

}

type FacturasMssqlRepository struct {
	DB *gorm.DB
	mu sync.RWMutex

}

func NewFacturaRepository(db *gorm.DB) *FacturasMssqlRepository {
	return &FacturasMssqlRepository{
		DB: db,
	}
}

func (f *FacturasMssqlRepository) Select() ([]models.FAC_Facturas,error) {
	result := []models.FAC_Facturas{}
	f.mu.RLock()
	rows, err := f.DB.Raw("SELECT * FROM BASE.FAC_FACTURAS").Rows() // (*sql.Rows, error)
	defer rows.Close()
	for rows.Next() {
		var row models.FAC_Facturas
		rows.Scan(&row.FAC_Id, &row.CLI_Id, &row.FAC_Fecha_Creacion,&row.FAC_Codigo_Factura,&row.FAC_Base_Imponible,&row.FAC_Impuesto,&row.FAC_Porcentage_Impuesto,&row.FAC_Monto_Total)
		dfaLst := []models.DFA_Detalles{}
		rowsDet, _ := f.DB.Raw("SELECT * FROM BASE.DFA_DETALLE_FACTURAS WHERE FAC_ID = ?",row.FAC_Id).Rows()
		defer rowsDet.Close()
		for rowsDet.Next() {
			var det models.DFA_Detalles
			rowsDet.Scan(&det.DFA_Id,&det.FAC_Id,&det.DFA_Descripcion,&det.DFA_Unidades,&det.DFA_Monto_Unitario,&det.DFA_Monto_Item)
			dfaLst = append(dfaLst,det)
		}
		row.DFA_Detalles = dfaLst
		result = append(result,row)
	}

	if err != nil {
		f.mu.RUnlock()
		return result,err
	}
	f.mu.RUnlock()
	return result,err
}

func (f *FacturasMssqlRepository) SelectById(id int) (models.FAC_Facturas,error) {
	result := models.FAC_Facturas{}
	f.mu.RLock()
	rows, err := f.DB.Raw("SELECT * FROM BASE.FAC_FACTURAS WHERE FAC_ID = ?", id).Rows() // (*sql.Rows, error)
	defer rows.Close()
	for rows.Next() {
		var row models.FAC_Facturas
		rows.Scan(&row.FAC_Id, &row.CLI_Id, &row.FAC_Fecha_Creacion,&row.FAC_Codigo_Factura,&row.FAC_Base_Imponible,&row.FAC_Impuesto,&row.FAC_Porcentage_Impuesto,&row.FAC_Monto_Total)
		dfaLst := []models.DFA_Detalles{}
		rowsDet, _ := f.DB.Raw("SELECT * FROM BASE.DFA_DETALLE_FACTURAS WHERE FAC_ID = ?",row.FAC_Id).Rows()
		defer rowsDet.Close()
		for rowsDet.Next() {
			var det models.DFA_Detalles
			rowsDet.Scan(&det.DFA_Id,&det.FAC_Id,&det.DFA_Descripcion,&det.DFA_Unidades,&det.DFA_Monto_Unitario,&det.DFA_Monto_Item)
			dfaLst = append(dfaLst,det)
		}
		row.DFA_Detalles = dfaLst
		result = row
	}
	if err != nil {
		f.mu.RUnlock()
		return result,err
	}
	f.mu.RUnlock()
	return result,err
}

func (f *FacturasMssqlRepository) SelectByNro(nro_factura string) ([]models.FAC_Facturas,error) {
	result := []models.FAC_Facturas{}
	f.mu.RLock()
	rows, err := f.DB.Raw("SELECT * FROM BASE.FAC_FACTURAS WHERE FAC_CODIGO_FACTURA LIKE ?", "%"+nro_factura+"%").Rows() // (*sql.Rows, error)
	defer rows.Close()
	for rows.Next() {
		var row models.FAC_Facturas
		rows.Scan(&row.FAC_Id, &row.CLI_Id, &row.FAC_Fecha_Creacion,&row.FAC_Codigo_Factura,&row.FAC_Base_Imponible,&row.FAC_Impuesto,&row.FAC_Porcentage_Impuesto,&row.FAC_Monto_Total)
		dfaLst := []models.DFA_Detalles{}
		rowsDet, _ := f.DB.Raw("SELECT * FROM BASE.DFA_DETALLE_FACTURAS WHERE FAC_ID = ?",row.FAC_Id).Rows()
		defer rowsDet.Close()
		for rowsDet.Next() {
			var det models.DFA_Detalles
			rowsDet.Scan(&det.DFA_Id,&det.FAC_Id,&det.DFA_Descripcion,&det.DFA_Unidades,&det.DFA_Monto_Unitario,&det.DFA_Monto_Item)
			dfaLst = append(dfaLst,det)
		}
		row.DFA_Detalles = dfaLst
		result = append(result,row)
	}

	if err != nil {
		f.mu.RUnlock()
		return result,err
	}
	f.mu.RUnlock()
	return result,err
}

func (f *FacturasMssqlRepository) Insert(factura models.FAC_Facturas) (result models.FAC_Facturas, err error) {
	f.mu.RLock()
	row := f.DB.Raw("SELECT CONCAT(CAST(ID_CLIENTE AS VARCHAR), '-', MAX(NEXTNUMBER)) AS NRO_FACTURA FROM (SELECT CLI.CLI_ID AS ID_CLIENTE, MAX(CAST(SUBSTRING(FAC.FAC_CODIGO_FACTURA, CHARINDEX('-',FAC.FAC_CODIGO_FACTURA)+1, 10) AS INT)) CURRNUMBER, ISNULL(CAST(SUBSTRING(FAC.FAC_CODIGO_FACTURA, CHARINDEX('-',FAC.FAC_CODIGO_FACTURA)+1, 10) AS INT), 0) + 1 NEXTNUMBER FROM BASE.CLI_CLIENTES CLI LEFT JOIN BASE.FAC_FACTURAS FAC ON CLI.CLI_ID = FAC.CLI_ID WHERE CLI.CLI_ID = ? GROUP BY CLI.CLI_ID, ISNULL(CAST(SUBSTRING(FAC.FAC_CODIGO_FACTURA, CHARINDEX('-',FAC.FAC_CODIGO_FACTURA)+1, 10) AS INT), 0) + 1) A GROUP BY ID_CLIENTE;", factura.CLI_Id).Row()
	row.Scan(&factura.FAC_Codigo_Factura)

	factura.FAC_Fecha_Creacion = time.Now()
	err = f.DB.Create(&factura).Error
	id := f.DB.Raw("SELECT MAX(FAC_ID) FROM BASE.FAC_FACTURAS;").Row()
	id.Scan(&result.FAC_Id)
	if err != nil {
		f.mu.RUnlock()
		return result, err
	}
	f.mu.RUnlock()
	f.mu.RLock()
	result,err = f.SelectById(result.FAC_Id)
	if err != nil {
		f.mu.RUnlock()
		return result,err
	}
	f.mu.RUnlock()
	return result, err
}

func (f *FacturasMssqlRepository) SelectByCodigoCli(codigoCli string) ([]models.FAC_Facturas,error) {
	result := []models.FAC_Facturas{}
	f.mu.RLock()

	rows, err := f.DB.Raw("SELECT FAC.* FROM BASE.CLI_CLIENTES CLI INNER JOIN BASE.FAC_FACTURAS FAC ON CLI.CLI_ID = FAC.CLI_ID WHERE CLI.CLI_STATUS = 1 AND CLI.CLI_CODIGO LIKE ?", "%"+codigoCli+"%").Rows() // (*sql.Rows, error)
	defer rows.Close()
	for rows.Next() {
		var row models.FAC_Facturas
		rows.Scan(&row.FAC_Id, &row.CLI_Id, &row.FAC_Fecha_Creacion,&row.FAC_Codigo_Factura,&row.FAC_Base_Imponible,&row.FAC_Impuesto,&row.FAC_Porcentage_Impuesto,&row.FAC_Monto_Total)
		dfaLst := []models.DFA_Detalles{}
		rowsDet, _ := f.DB.Raw("SELECT * FROM BASE.DFA_DETALLE_FACTURAS WHERE FAC_ID = ?",row.FAC_Id).Rows()
		defer rowsDet.Close()
		for rowsDet.Next() {
			var det models.DFA_Detalles
			rowsDet.Scan(&det.DFA_Id,&det.FAC_Id,&det.DFA_Descripcion,&det.DFA_Unidades,&det.DFA_Monto_Unitario,&det.DFA_Monto_Item)
			dfaLst = append(dfaLst,det)
		}
		row.DFA_Detalles = dfaLst
		result = append(result,row)
	}
	if err != nil {
		f.mu.RUnlock()
		return result, err
	}
	f.mu.RUnlock()
	return result,err

}

func (f *FacturasMssqlRepository) SelectByNombreCli(nombreCli string) ([]models.FAC_Facturas,error) {
	result := []models.FAC_Facturas{}
	f.mu.RLock()

	rows, err := f.DB.Raw("SELECT FAC.* FROM BASE.CLI_CLIENTES CLI INNER JOIN BASE.FAC_FACTURAS FAC ON CLI.CLI_ID = FAC.CLI_ID WHERE CLI.CLI_STATUS = 1 AND CLI.CLI_NOMBRE LIKE ?", "%"+nombreCli+"%").Rows() // (*sql.Rows, error)
	defer rows.Close()
	for rows.Next() {
		var row models.FAC_Facturas
		rows.Scan(&row.FAC_Id, &row.CLI_Id, &row.FAC_Fecha_Creacion,&row.FAC_Codigo_Factura,&row.FAC_Base_Imponible,&row.FAC_Impuesto,&row.FAC_Porcentage_Impuesto,&row.FAC_Monto_Total)
		dfaLst := []models.DFA_Detalles{}
		rowsDet, _ := f.DB.Raw("SELECT * FROM BASE.DFA_DETALLE_FACTURAS WHERE FAC_ID = ?",row.FAC_Id).Rows()
		defer rowsDet.Close()
		for rowsDet.Next() {
			var det models.DFA_Detalles
			rowsDet.Scan(&det.DFA_Id,&det.FAC_Id,&det.DFA_Descripcion,&det.DFA_Unidades,&det.DFA_Monto_Unitario,&det.DFA_Monto_Item)
			dfaLst = append(dfaLst,det)
		}
		row.DFA_Detalles = dfaLst
		result = append(result,row)
	}


	if err != nil {
		f.mu.RUnlock()
		return result, err
	}
	f.mu.RUnlock()
	return result,err
}

func (f *FacturasMssqlRepository) SelectByFechas(fechaIni string, fechaFin string) ([]models.FAC_Facturas,error) {
	fDesde := strings.Split(fechaIni, "-")
	fHasta := strings.Split(fechaFin, "-")

	Ini, _ := strconv.Atoi(fDesde[2]+fDesde[1]+fDesde[0])
	Fin, _ := strconv.Atoi(fHasta[2]+fHasta[1]+fHasta[0])

	result := []models.FAC_Facturas{}
	f.mu.RLock()

	err := f.DB.Preload("Detalles").Where("fecha_emision between ? AND ?", Ini, Fin).Find(&result).Error
	if err != nil {
		f.mu.RUnlock()
		return result, err
	}
	f.mu.RUnlock()
	return result,err

}

func (f *FacturasMssqlRepository) SelectByConcepto(concepto string) ([]models.FAC_Facturas,error) {
	f.mu.RLock()
	result := []models.FAC_Facturas{}

	rows, err := f.DB.Raw("SELECT FAC.* FROM BASE.FAC_FACTURAS FAC LEFT JOIN BASE.DFA_DETALLE_FACTURAS DFA ON FAC.FAC_ID = DFA.FAC_ID WHERE DFA.DFA_DESCRIPCION LIKE ?", "%"+concepto+"%").Rows() // (*sql.Rows, error)
	defer rows.Close()
	for rows.Next() {
		var row models.FAC_Facturas
		rows.Scan(&row.FAC_Id, &row.CLI_Id, &row.FAC_Fecha_Creacion,&row.FAC_Codigo_Factura,&row.FAC_Base_Imponible,&row.FAC_Impuesto,&row.FAC_Porcentage_Impuesto,&row.FAC_Monto_Total)
		dfaLst := []models.DFA_Detalles{}
		rowsDet, _ := f.DB.Raw("SELECT * FROM BASE.DFA_DETALLE_FACTURAS WHERE FAC_ID = ?",row.FAC_Id).Rows()
		defer rowsDet.Close()
		for rowsDet.Next() {
			var det models.DFA_Detalles
			rowsDet.Scan(&det.DFA_Id,&det.FAC_Id,&det.DFA_Descripcion,&det.DFA_Unidades,&det.DFA_Monto_Unitario,&det.DFA_Monto_Item)
			dfaLst = append(dfaLst,det)
		}
		row.DFA_Detalles = dfaLst
		result = append(result,row)
	}

	if err != nil {
		f.mu.RUnlock()
		return result, err
	}
	f.mu.RUnlock()
	return result,err
}





