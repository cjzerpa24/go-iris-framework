package repos

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"
	"go_iris/models"
	"sync"
)

type ClienteRepository interface {
	Select() ([]models.CLI_Cliente,error)
	SelectById(id int) (models.CLI_Cliente,error)
	SelectByName(nombre string) ([]models.CLI_Cliente,error)
	SelectByCodigo(codigo string) ([]models.CLI_Cliente,error)
	SelectByCodigoyName(codigo string,name string) ([]models.CLI_Cliente,error)
	Insert(cliente models.CLI_Cliente) (models.CLI_Cliente, error)
	Update(cliente models.CLI_Cliente) (models.CLI_Cliente,error)
	Delete(cliente []models.CLI_Cliente) (bool,int)
	UpdateCliente(cliente []models.CLI_Cliente) (bool,int)

}

type ClienteMssqlRepository struct {
	DB *gorm.DB
	mu sync.RWMutex

}

func NewClienteRepository(db *gorm.DB) *ClienteMssqlRepository {
	return &ClienteMssqlRepository{
		DB: db,
	}
}

func (c *ClienteMssqlRepository) Select() ([]models.CLI_Cliente,error) {
	result := []models.CLI_Cliente{}

	c.mu.RLock()
	rows, err := c.DB.Raw("SELECT CLI_Id id, CLI_Codigo codigo, CLI_Nombre nombre, CLI_Direccion direccion, CLI_Correo correo, CLI_Telefono telefono, CLI_Status status FROM BASE.CLI_CLIENTES WHERE CLI_Status = 1").Rows() // (*sql.Rows, error)
	defer rows.Close()
	for rows.Next() {
		var row models.CLI_Cliente
		rows.Scan(&row.CLI_Id, &row.CLI_Codigo, &row.CLI_Nombre,&row.CLI_Direccion,&row.CLI_Correo,&row.CLI_Telefono,&row.CLI_Status)
		result = append(result,row)
	}
	if err != nil {
		c.mu.RUnlock()
		return result, err
	}
	c.mu.RUnlock()

	return result,err
}

func (c *ClienteMssqlRepository) SelectById(id int) (models.CLI_Cliente,error) {
	result := models.CLI_Cliente{}
	c.mu.RLock()
	rows,err := c.DB.Raw("SELECT CLI_Id id, CLI_Codigo codigo, CLI_Nombre nombre, CLI_Direccion direccion, CLI_Correo correo, CLI_Telefono telefono, CLI_Status status FROM BASE.CLI_CLIENTES WHERE CLI_ID = ?", id).Rows()
	defer rows.Close()
	for rows.Next() {
		var row models.CLI_Cliente
		rows.Scan(&row.CLI_Id, &row.CLI_Codigo, &row.CLI_Nombre,&row.CLI_Direccion,&row.CLI_Correo,&row.CLI_Telefono,&row.CLI_Status)
		result = row
	}
	if err != nil {
		c.mu.RUnlock()
		return result, err
	}

	c.mu.RUnlock()
	return result,err
}

func (c *ClienteMssqlRepository) SelectByCodigo(codigo string) ([]models.CLI_Cliente,error) {
	result := make([]models.CLI_Cliente,0)
	c.mu.RLock()
	rows, err := c.DB.Raw("SELECT CLI_Id id, CLI_Codigo codigo, CLI_Nombre nombre, CLI_Direccion direccion, CLI_Correo correo, CLI_Telefono telefono, CLI_Status status FROM BASE.CLI_CLIENTES WHERE CLI_STATUS = 1 AND CLI_CODIGO LIKE ?","%"+codigo+"%").Rows()
	defer rows.Close()
	for rows.Next() {
		var row models.CLI_Cliente
		rows.Scan(&row.CLI_Id, &row.CLI_Codigo, &row.CLI_Nombre,&row.CLI_Direccion,&row.CLI_Correo,&row.CLI_Telefono,&row.CLI_Status)
		result = append(result,row)
	}
	if err != nil {
		c.mu.RUnlock()
		return result, err
	}
	c.mu.RUnlock()
	return result,err
}

func (c *ClienteMssqlRepository) SelectByName(nombre string)  ([]models.CLI_Cliente,error) {
	result := []models.CLI_Cliente{}
	c.mu.RLock()
	rows, err := c.DB.Raw("SELECT CLI_Id id, CLI_Codigo codigo, CLI_Nombre nombre, CLI_Direccion direccion, CLI_Correo correo, CLI_Telefono telefono, CLI_Status status FROM BASE.CLI_CLIENTES WHERE CLI_STATUS = 1 AND CLI_NOMBRE LIKE ?","%"+nombre+"%").Rows()
	defer rows.Close()
	for rows.Next() {
		var row models.CLI_Cliente
		rows.Scan(&row.CLI_Id, &row.CLI_Codigo, &row.CLI_Nombre,&row.CLI_Direccion,&row.CLI_Correo,&row.CLI_Telefono,&row.CLI_Status)
		result = append(result,row)
	}
	if err != nil {
		c.mu.RUnlock()
		return result, err
	}
	c.mu.RUnlock()
	return result,err
}

func (c *ClienteMssqlRepository) SelectByCodigoyName(nombre string, codigo string) ([]models.CLI_Cliente,error) {
	result := []models.CLI_Cliente{}
	c.mu.RLock()
	rows, err := c.DB.Raw("SELECT CLI_Id id, CLI_Codigo codigo, CLI_Nombre nombre, CLI_Direccion direccion, CLI_Correo correo, CLI_Telefono telefono, CLI_Status status FROM BASE.CLI_CLIENTES WHERE CLI_STATUS = 1 AND CLI_CODIGO LIKE ? AND CLI_NOMBRE LIKE ?","%"+codigo+"%", "%"+nombre+"%").Rows()
	defer rows.Close()
	for rows.Next() {
		var row models.CLI_Cliente
		rows.Scan(&row.CLI_Id, &row.CLI_Codigo, &row.CLI_Nombre,&row.CLI_Direccion,&row.CLI_Correo,&row.CLI_Telefono,&row.CLI_Status)
		result = append(result,row)
	}
	if err != nil {
		c.mu.RUnlock()
		return result, err
	}
	c.mu.RUnlock()
	return result,err
}


func (c *ClienteMssqlRepository) Insert(cli models.CLI_Cliente) (result models.CLI_Cliente, err error) {
	c.mu.RLock()
	err = c.DB.Create(&cli).Error
	row := c.DB.Raw("SELECT MAX(CLI_ID) FROM BASE.CLI_CLIENTES;").Row()
	row.Scan(&result.CLI_Id)
	if err != nil {
		c.mu.RUnlock()
		return result, err
	}
	c.mu.RUnlock()
	c.mu.RLock()
	result, err = c.SelectById(result.CLI_Id)
	if err != nil {
		c.mu.RUnlock()
		return result, err
	}
	c.mu.RUnlock()
	return result,err
}

func (c *ClienteMssqlRepository) Update(cli models.CLI_Cliente) (result models.CLI_Cliente, err error) {
	c.mu.RLock()
	id := cli.CLI_Id
	cli.CLI_Id = 0
	err = c.DB.Model(&result).Where("CLI_ID = ?", id).Updates(cli).Error
	if err != nil {
		c.mu.RUnlock()
		return result, err
	}
	c.mu.RUnlock()
	c.mu.RLock()
	err = c.DB.Where("CLI_ID = ?", id).Find(&result).Error
	result.CLI_Id = id
	if err != nil {
		c.mu.RUnlock()
		return result,err
	}
	c.mu.RUnlock()
	return result, err
}

func (c *ClienteMssqlRepository) Delete(cli []models.CLI_Cliente) (bool,int) {
	var cont int
	for _, s := range cli {
		c.mu.RLock()
		err := c.DB.Delete(&models.CLI_Cliente{}, "ID = ?",s.CLI_Id).Error
		c.mu.RUnlock()
		if err != nil {
			cont = 0
		}else {
			cont++
		}
	}
	if cont == 0 {
		return false,cont
	}
	return true,cont
}

func (c *ClienteMssqlRepository) UpdateCliente(cliente []models.CLI_Cliente) (bool,int) {
	var cont int

	for _,s := range cliente {
		c.mu.RLock()
		err := c.DB.Model(&models.CLI_Cliente{}).Where("CLI_ID = ?", s.CLI_Id).UpdateColumn("cli_status",0).Error
		c.mu.RUnlock()
		if err != nil {
			cont = 0
		} else {
			cont++
		}
	}
	if cont == 0 {
		return false, cont
	}
	return true, cont
}



