package repos

import (
	"github.com/jinzhu/gorm"
	"go_iris/models"
	"sync"
)

type DetallesRepository interface {
	SelectById(id int) (models.DFA_Detalles,error)
	SelectByIdFactura(idFactura int) ([]models.DFA_Detalles,error)
	Insert(detalles models.DFA_Detalles) (result models.DFA_Detalles, err error)
	Delete(detalles []models.DFA_Detalles) (bool, int)
	sumatoriaBI(id_factura uint) float64
	Update(detalles models.DFA_Detalles) (result models.DFA_Detalles,err error)
}

func FacturasRepository(db *gorm.DB) *FacturasMssqlRepository {
	return &FacturasMssqlRepository{
		DB: db,
		mu: sync.RWMutex{},
	}
}

type DetallesMssqlRepository struct {
	DB *gorm.DB
	mu sync.RWMutex

}

func NewDetalleRepository(db *gorm.DB) *DetallesMssqlRepository {
	return &DetallesMssqlRepository{
		DB: db,
	}
}

//Get Detalle By Id
func (d *DetallesMssqlRepository) SelectById(id int) (models.DFA_Detalles,error) {
	result := models.DFA_Detalles{}
	d.mu.RLock()

	rows, err :=d.DB.Raw("SELECT DFA.* FROM BASE.DFA_DETALLE_FACTURAS DFA WHERE DFA.DFA_ID = ?", id).Rows()
	defer rows.Close()
	for rows.Next() {
		var row models.DFA_Detalles
		rows.Scan(&row.DFA_Id, &row.FAC_Id, &row.DFA_Descripcion,&row.DFA_Unidades,&row.DFA_Monto_Unitario,&row.DFA_Monto_Item)

		result = row
	}

	if err != nil {
		d.mu.RUnlock()
		return result,err
	}
	d.mu.RUnlock()
	return result,err
}

//Get By Detalles By Factura
func (d *DetallesMssqlRepository) SelectByIdFactura(idFactura int) ([]models.DFA_Detalles,error) {
	result := []models.DFA_Detalles{}
	d.mu.RLock()

	rows, err :=d.DB.Raw("SELECT DFA.* FROM BASE.DFA_DETALLE_FACTURAS DFA WHERE DFA.FAC_ID = ?", idFactura).Rows()
	defer rows.Close()
	for rows.Next() {
		var row models.DFA_Detalles
		rows.Scan(&row.DFA_Id, &row.FAC_Id, &row.DFA_Descripcion,&row.DFA_Unidades,&row.DFA_Monto_Unitario,&row.DFA_Monto_Item)

		result = append(result,row)
	}

	if err != nil {
		d.mu.RUnlock()
		return result,err
	}
	d.mu.RUnlock()
	return result,err
}

//Obtener la suma de los detalles por Factura
func (d *DetallesMssqlRepository) sumatoriaBI(id_factura uint) float64 {
	var result float64
	row := d.DB.Table("BASE.DFA_DETALLE_FACTURAS").Where("FAC_ID = ?", id_factura).Select("isnull(sum(DFA_Monto_Item),0)").Row()
	row.Scan(&result)
	return result
}

//Crear nuevo Detalle
func (d *DetallesMssqlRepository) Insert(detalles models.DFA_Detalles) (result models.DFA_Detalles, err error) {
	d.mu.RLock()
	detalles.DFA_Monto_Item = detalles.DFA_Monto_Unitario * detalles.DFA_Unidades
	err = d.DB.Create(&detalles).Error
	if err != nil {
		d.mu.RUnlock()
		return result, err
	}
	d.mu.RUnlock()
	d.mu.RLock()
	factura := models.FAC_Facturas{}
	factura, _ = FacturasRepository(d.DB).SelectById(detalles.FAC_Id)
	d.DB.Table("BASE.FAC_FACTURAS").Where("FAC_ID = ?", detalles.FAC_Id).UpdateColumn("FAC_BASE_IMPONIBLE",d.sumatoriaBI(uint(factura.FAC_Id)))
	d.DB.Table("BASE.FAC_FACTURAS").Where("FAC_ID = ?", detalles.FAC_Id).UpdateColumn("FAC_IMPUESTO",d.sumatoriaBI(uint(factura.FAC_Id)) * factura.FAC_Porcentage_Impuesto * 0.01)
	d.DB.Table("BASE.FAC_FACTURAS").Where("FAC_ID = ?", detalles.FAC_Id).UpdateColumn("FAC_MONTO_TOTAL",(d.sumatoriaBI(uint(factura.FAC_Id)) + d.sumatoriaBI(uint(factura.FAC_Id)) * factura.FAC_Porcentage_Impuesto * 0.01))
	d.mu.RUnlock()
	d.mu.RLock()
	result, err = d.SelectById(detalles.DFA_Id)
	if err != nil {
		d.mu.RUnlock()
		return result,err
	}
	d.mu.RUnlock()
	return result, err
}

//Delete Detalle By Ids
func (d *DetallesMssqlRepository) Delete(detalles []models.DFA_Detalles) (bool, int) {
	var cont int
	for _, s := range detalles {
		d.mu.RLock()

		det := models.DFA_Detalles{}
		det,_=d.SelectById(s.DFA_Id)

		err := d.DB.Delete(&models.DFA_Detalles{}, "DFA_ID = ?",s.DFA_Id).Error

		factura := models.FAC_Facturas{}
		factura, _ = FacturasRepository(d.DB).SelectById(det.FAC_Id)
		d.DB.Table("BASE.FAC_FACTURAS").Where("FAC_ID = ?", det.FAC_Id).UpdateColumn("FAC_BASE_IMPONIBLE",d.sumatoriaBI(uint(factura.FAC_Id)))
		d.DB.Table("BASE.FAC_FACTURAS").Where("FAC_ID = ?", det.FAC_Id).UpdateColumn("FAC_IMPUESTO",d.sumatoriaBI(uint(factura.FAC_Id)) * factura.FAC_Porcentage_Impuesto * 0.01)
		d.DB.Table("BASE.FAC_FACTURAS").Where("FAC_ID = ?", det.FAC_Id).UpdateColumn("FAC_MONTO_TOTAL",(d.sumatoriaBI(uint(factura.FAC_Id)) + d.sumatoriaBI(uint(factura.FAC_Id)) * factura.FAC_Porcentage_Impuesto * 0.01))

		d.mu.RUnlock()
		if err != nil {
			cont = 0
		}else {
			cont++
		}
	}
	if cont == 0 {
		return false,cont
	}
	return true,cont
}

//Update Detalle By Id
func (d *DetallesMssqlRepository) Update(detalles models.DFA_Detalles) (result models.DFA_Detalles,err error) {
	d.mu.RLock()
	id := detalles.DFA_Id
	det := models.DFA_Detalles{}
	det,_ = d.SelectById(id)
	if detalles.DFA_Unidades != 0 && detalles.DFA_Monto_Unitario != 0 {
		detalles.DFA_Monto_Item = detalles.DFA_Unidades * detalles.DFA_Monto_Unitario
	}
	if detalles.DFA_Unidades != 0 && detalles.DFA_Monto_Unitario == 0 {
		detalles.DFA_Monto_Item = detalles.DFA_Unidades * det.DFA_Monto_Unitario
	}
	if detalles.DFA_Unidades == 0 && detalles.DFA_Monto_Unitario != 0 {
		detalles.DFA_Monto_Item = det.DFA_Unidades * detalles.DFA_Monto_Unitario
	}
	detalles.DFA_Id = 0
	err = d.DB.Table("BASE.DFA_DETALLE_FACTURAS").Where("DFA_ID = ?", id).Updates(detalles).Error
	if err != nil {
		d.mu.RUnlock()
		return result, err
	}
	d.mu.RUnlock()
	d.mu.RLock()

	//Update Montos de Factura
	factura := models.FAC_Facturas{}
	factura, _ = FacturasRepository(d.DB).SelectById(det.FAC_Id)
	d.DB.Table("BASE.FAC_FACTURAS").Where("FAC_ID = ?", det.FAC_Id).UpdateColumn("FAC_BASE_IMPONIBLE",d.sumatoriaBI(uint(factura.FAC_Id)))
	d.DB.Table("BASE.FAC_FACTURAS").Where("FAC_ID = ?", det.FAC_Id).UpdateColumn("FAC_IMPUESTO",d.sumatoriaBI(uint(factura.FAC_Id)) * factura.FAC_Porcentage_Impuesto * 0.01)
	d.DB.Table("BASE.FAC_FACTURAS").Where("FAC_ID = ?", det.FAC_Id).UpdateColumn("FAC_MONTO_TOTAL",(d.sumatoriaBI(uint(factura.FAC_Id)) + d.sumatoriaBI(uint(factura.FAC_Id)) * factura.FAC_Porcentage_Impuesto * 0.01))


	result, err = d.SelectById(id)
	if err != nil {
		d.mu.RUnlock()
		return result,err
	}
	d.mu.RUnlock()
	return result, err
}
