package services

import (
	"go_iris/models"
	"go_iris/repos"
)

type FacturasService interface {
	GetAll() ([]models.FAC_Facturas,error)
	GetById(id int) (models.FAC_Facturas,error)
	GetByCodigo(nro string) ([]models.FAC_Facturas,error)
	GetByCodigoCli(codigoCli string) ([]models.FAC_Facturas,error)
	GetByNombreCli(nombreCli string) ([]models.FAC_Facturas,error)
	GetByFechas(fechaIni string,fechaFin string) ([]models.FAC_Facturas,error)
	GetByConcepto(concepto string) ([]models.FAC_Facturas,error)
	Save(facturas models.FAC_Facturas) (models.FAC_Facturas,error)
}

type facturaService struct {
	repo repos.FacturaRepository
}

func NewFacturasService(repo *repos.FacturasMssqlRepository) *facturaService {
	return &facturaService{
		repo: repo,
	}
}

func (f *facturaService) GetAll() ([]models.FAC_Facturas,error) {
	return f.repo.Select()
}

func (f *facturaService) GetById(id int) (models.FAC_Facturas,error) {
	return f.repo.SelectById(id)
}

func (f *facturaService) GetByCodigo(nro string) ([]models.FAC_Facturas,error) {
	return f.repo.SelectByNro(nro)
}

func (f *facturaService) GetByCodigoCli(codigoCli string) ([]models.FAC_Facturas,error) {
	return f.repo.SelectByCodigoCli(codigoCli)
}

func (f *facturaService) GetByNombreCli(nombreCli string) ([]models.FAC_Facturas,error) {
	return f.repo.SelectByNombreCli(nombreCli)
}

func (f *facturaService) GetByFechas(fechaIni string,fechaFin string) ([]models.FAC_Facturas,error) {
	return f.repo.SelectByFechas(fechaIni,fechaFin)
}

func (f *facturaService) GetByConcepto(concepto string) ([]models.FAC_Facturas,error) {
	return f.repo.SelectByConcepto(concepto)
}

func (f *facturaService) Save(fac models.FAC_Facturas) (models.FAC_Facturas,error) {
	return f.repo.Insert(fac)
}





