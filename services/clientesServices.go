package services

import (
	"go_iris/models"
	"go_iris/repos"
)

type ClientesService interface {
	GetAll() ([]models.CLI_Cliente,error)
	GetById(id int) (models.CLI_Cliente,error)
	GetByName(name string) ([]models.CLI_Cliente,error)
	GetByCodigo(codigo string) ([]models.CLI_Cliente,error)
	GetByCodigoyName(codigo string, nombre string) ([]models.CLI_Cliente,error)
	Save(cli models.CLI_Cliente) (models.CLI_Cliente,error)
	UpdateById(cli models.CLI_Cliente) (models.CLI_Cliente,error)
	DeleteById(cli []models.CLI_Cliente) (bool,int)
	DeleteByUpdate(cli []models.CLI_Cliente) (bool,int)
}

type clienteService struct {
	repo repos.ClienteRepository
}

func NewClientesService(repo *repos.ClienteMssqlRepository) *clienteService {
	return &clienteService{
		repo: repo,
	}
}

func (s *clienteService) GetAll() ([]models.CLI_Cliente,error) {
	return s.repo.Select()
}

func (s *clienteService) GetById(id int) (models.CLI_Cliente,error) {
	return s.repo.SelectById(id)
}

func (s *clienteService) GetByName(nombre string) ([]models.CLI_Cliente,error) {
	return s.repo.SelectByName(nombre)
}

func (s *clienteService) GetByCodigo(codigo string) ([]models.CLI_Cliente,error) {
	return s.repo.SelectByCodigo(codigo)
}

func (s *clienteService) GetByCodigoyName(codigo string, nombre string) ([]models.CLI_Cliente,error) {
	return s.repo.SelectByCodigoyName(nombre,codigo)
}

func (s *clienteService) Save(cli models.CLI_Cliente) (models.CLI_Cliente,error) {
	return s.repo.Insert(cli)
}

func (s *clienteService) UpdateById(cli models.CLI_Cliente) (models.CLI_Cliente,error) {
	return s.repo.Update(cli)
}


func (s *clienteService) DeleteById(cli []models.CLI_Cliente) (bool,int) {
	return s.repo.Delete(cli)
}

func (s *clienteService) DeleteByUpdate(cli []models.CLI_Cliente) (bool,int) {
	return s.repo.UpdateCliente(cli)
}

