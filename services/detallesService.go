package services

import (
	"go_iris/models"
	"go_iris/repos"
)

type DetallesService interface {
	GetById(id int) (models.DFA_Detalles,error)
	GetByFactura(idFactura int) ([]models.DFA_Detalles,error)
	Save(detalle models.DFA_Detalles) (models.DFA_Detalles,error)
	Remove(detalles []models.DFA_Detalles) (bool,int)
	UpdateBy(detalle models.DFA_Detalles) (models.DFA_Detalles,error)
}

type detalleService struct {
	repo repos.DetallesRepository
}

func NewDetallesService(repo *repos.DetallesMssqlRepository) *detalleService {
	return &detalleService{
		repo: repo,
	}
}

func (d *detalleService) GetById(id int) (models.DFA_Detalles,error) {
	return d.repo.SelectById(id)
}

func (d *detalleService) GetByFactura(idFactura int) ([]models.DFA_Detalles,error) {
	return d.repo.SelectByIdFactura(idFactura)
}

func (d *detalleService) Save(detalle models.DFA_Detalles) (models.DFA_Detalles,error) {
	return d.repo.Insert(detalle)
}

func (d *detalleService) Remove(detalles []models.DFA_Detalles) (bool,int) {
	return d.repo.Delete(detalles)
}

func (d *detalleService) UpdateBy(detalle models.DFA_Detalles) (models.DFA_Detalles,error) {
	return d.repo.Update(detalle)
}