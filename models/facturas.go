package models

import "time"

type FAC_Facturas struct {
	FAC_Id	int `gorm:"primary_key" json:"id"`
	CLI_Id	int `json:"cliId"`
	FAC_Fecha_Creacion  time.Time `json:"fecha_emision"`
	FAC_Codigo_Factura 	string `json:"nro_factura"`
	FAC_Base_Imponible		float64	`json:"subtotal"`
	FAC_Impuesto	float64	`json:"impuesto"`
	FAC_Porcentage_Impuesto float64	`json:"porcentaje"`
	FAC_Monto_Total	float64	` json:"total"`
	DFA_Detalles []DFA_Detalles
}

func (FAC_Facturas) TableName() string {
	return "BASE.FAC_Facturas"
}
