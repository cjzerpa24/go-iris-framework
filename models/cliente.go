package models

type CLI_Cliente struct {
	CLI_Id	int	`gorm:"primary_key" json:"id"`
	CLI_Codigo	string	` json:"codigo"`
	CLI_Nombre 	string	` json:"nombre"`
	CLI_Direccion string	` json:"direccion"`
	CLI_Correo	string		` json:"correo"`
	CLI_Telefono string 	` json:"telefono"`
	CLI_Status	int		` json:"estatus"`
	//Facturas []FAC_Facturas  `gorm:"foreignkey:CLI_Id"`
}

func (CLI_Cliente) TableName() string {
	return "BASE.CLI_CLIENTES"
}
