package models

type DFA_Detalles struct {
	DFA_Id	int	`gorm:"primary_key" json:"id"`
	FAC_Id	int	`json:"idfactura"`
	DFA_Descripcion string	`json:"descripcion"`
	DFA_Unidades 	float64	`json:"unidades"`
	DFA_Monto_Unitario	float64	`json:"monto_unitario"`
	DFA_Monto_Item	float64	`json:"monto_item"`
}

func (DFA_Detalles) TableName() string {
	return "BASE.DFA_Detalle_Facturas"
}

