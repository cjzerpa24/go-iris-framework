package handlers

import (
	"github.com/kataras/iris"
	"github.com/kataras/iris/context"
	"go_iris/models"
	"go_iris/services"
)

type FacturaHandler struct {
	factura services.FacturasService
}

func NewFacturaHandler(facturas services.FacturasService) *FacturaHandler {
	return &FacturaHandler{factura: facturas}
}

/*
	FACTURAS
*/

func (f *FacturaHandler) GetAll(ctx iris.Context) {
	fac,err := f.factura.GetAll()
	if err != nil {
		ctx.StatusCode(404)
		ctx.JSON(context.Map{"response": "No existen registros"})
	}else {
		ctx.StatusCode(200)
		ctx.JSON(context.Map{"response": fac})
	}
}

func (f *FacturaHandler) GetById(ctx iris.Context) {
	idfactura, _ := ctx.Params().GetInt("id")
	fac,err := f.factura.GetById(idfactura)
	if err != nil {
		ctx.StatusCode(404)
		ctx.JSON(context.Map{"response": "No existen registros"})
	} else {
		ctx.StatusCode(200)
		ctx.JSON(context.Map{"response": fac})
	}
}

func (f *FacturaHandler) GetByCodigoFactura(ctx iris.Context) {
	nroFactura := ctx.Params().Get("factura")
	fac,err := f.factura.GetByCodigo(nroFactura)
	if err != nil {
		ctx.StatusCode(404)
		ctx.JSON(context.Map{"response": "No existen registros"})
	} else {
		ctx.StatusCode(200)
		ctx.JSON(context.Map{"response": fac})
	}
}

func (f *FacturaHandler) GetByCodigoCliente(ctx iris.Context) {
	codigoCli := ctx.Params().Get("codigoCliente")
	fac, err := f.factura.GetByCodigoCli(codigoCli)
	if err != nil {
		ctx.StatusCode(404)
		ctx.JSON(context.Map{"response": "No existen registros"})
	} else {
		ctx.StatusCode(200)
		ctx.JSON(context.Map{"response": fac})
	}
}

func (f *FacturaHandler) GetByNombreCliente(ctx iris.Context) {
	nombreCli := ctx.Params().Get("nombreCliente")
	fac, err := f.factura.GetByNombreCli(nombreCli)
	if err != nil {
		ctx.StatusCode(404)
		ctx.JSON(context.Map{"response": "No existen registros"})
	} else {
		ctx.StatusCode(200)
		ctx.JSON(context.Map{"response": fac})
	}
}

func (f *FacturaHandler) GetByConcepto(ctx iris.Context) {
	concepto := ctx.Params().Get("concepto")
	fac,err := f.factura.GetByConcepto(concepto)
	if err != nil {
		ctx.StatusCode(404)
		ctx.JSON(context.Map{"response": "No existen registros"})
	} else {
		ctx.StatusCode(200)
		ctx.JSON(context.Map{"response": fac})
	}
}

func (f *FacturaHandler) AddFactura(ctx iris.Context) {
	params := models.FAC_Facturas{}
	err := ctx.ReadJSON(&params)
	if err != nil {
		ctx.JSON(context.Map{"response": err.Error()})
	} else {
		result := models.FAC_Facturas{}
		result, err := f.factura.Save(params)
		if err != nil {
			ctx.StatusCode(404)
			ctx.JSON(context.Map{"response": err.Error()})
		} else {
			ctx.StatusCode(200)
			ctx.JSON(context.Map{"message": "Factura creada Exitosamente", "response": result})
		}
	}
}

func (f *FacturaHandler) UpdateFactura(ctx iris.Context) {
	params := models.FAC_Facturas{}
	err := ctx.ReadJSON(&params)
	if err != nil {
		ctx.JSON(context.Map{"response": err.Error()})
	} else {
		ctx.StatusCode(403)
		ctx.JSON(context.Map{"response": "PUT operation not supported on /facturas"})
	}
}

func (f *FacturaHandler) DeleteFactura(ctx iris.Context) {
	params := models.FAC_Facturas{}
	err := ctx.ReadJSON(&params)
	if err != nil {
		ctx.JSON(context.Map{"response": err.Error()})
	} else {
		ctx.StatusCode(403)
		ctx.JSON(context.Map{"response": "DELETE operation not supported on /facturas"})
	}
}