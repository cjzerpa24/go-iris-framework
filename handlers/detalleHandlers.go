package handlers

import (
	"fmt"
	"github.com/kataras/iris"
	"github.com/kataras/iris/context"
	"go_iris/models"
	"go_iris/services"
)

type DetalleHandler struct {
	detalle services.DetallesService
}

func NewDetalleHandler(detalle services.DetallesService) *DetalleHandler {
	return &DetalleHandler{detalle: detalle}
}

/*
	DETALLES
*/

func (d *DetalleHandler) GetAll(ctx iris.Context) {
	id_factura, _ := ctx.Params().GetInt("id")
	detalles, err := d.detalle.GetByFactura(id_factura)
	if err != nil {
		ctx.StatusCode(404)
		ctx.JSON(context.Map{"response": "No existen registros"})
	}else {
		ctx.StatusCode(200)
		ctx.JSON(context.Map{"response": detalles})
	}
}

func (d *DetalleHandler) GetById(ctx iris.Context) {
	id_detalle, _ :=ctx.Params().GetInt("id_detalle")
	detalles, err := d.detalle.GetById(id_detalle)
	if err != nil {
		ctx.StatusCode(404)
		ctx.JSON(context.Map{"response": "No existen registros"})
	}else {
		ctx.StatusCode(200)
		ctx.JSON(context.Map{"response": detalles})
	}
}

func (d *DetalleHandler) AddDetalle(ctx iris.Context) {
	params := models.DFA_Detalles{}
	err := ctx.ReadJSON(&params)
	params.FAC_Id, _ = ctx.Params().GetInt("id")
	if err != nil {
		ctx.JSON(context.Map{"response": err.Error()})
	} else {
		result := models.DFA_Detalles{}
		result, err := d.detalle.Save(params)
		if err != nil {
			ctx.StatusCode(404)
			ctx.JSON(context.Map{"response": err.Error()})
		} else {
			ctx.StatusCode(200)
			ctx.JSON(context.Map{"message": "Item factura creado Exitosamente", "response": result})

		}
	}
}

func (d *DetalleHandler) DeleteDetalle(ctx iris.Context) {
	params := []models.DFA_Detalles{}
	err := ctx.ReadJSON(&params)
	if err != nil {
		ctx.JSON(context.Map{"response": err.Error()})
	} else {
		_,cant := d.detalle.Remove(params)
		if err != nil {
			ctx.StatusCode(404)
			ctx.JSON(context.Map{"response": err.Error()})
		} else {
			message := fmt.Sprintf("Se eliminaron %d registros exitosamente", cant)
			ctx.StatusCode(200)
			ctx.JSON(context.Map{"message": message})

		}
	}
}

func (d *DetalleHandler) UpdateDetalle (ctx iris.Context) {
	params := models.DFA_Detalles{}
	iditem, _ := ctx.Params().GetInt("iditem")
	idfactura, _ := ctx.Params().GetInt("idfactura")
	err := ctx.ReadJSON(&params)
	params.DFA_Id = iditem
	params.FAC_Id = idfactura
	if err != nil {
		ctx.JSON(context.Map{"response": err.Error()})
	} else {
		result := models.DFA_Detalles{}
		result, err := d.detalle.UpdateBy(params)
		if err != nil {
			ctx.StatusCode(500)
			ctx.JSON(context.Map{"response": err.Error()})
		} else {
			ctx.StatusCode(200)
			ctx.JSON(context.Map{"message": "Detalle actualizado exitosamente", "response": result})
		}
	}
}