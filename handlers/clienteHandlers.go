package handlers

import (
	"fmt"
	"github.com/kataras/iris"
	"github.com/kataras/iris/context"
	"go_iris/models"
	"go_iris/services"
)

type ClienteHandler struct {
	cliente services.ClientesService
}

func NewClienteHandler(cliente services.ClientesService) *ClienteHandler {
	return &ClienteHandler{cliente: cliente}
}

/*
	CLIENTES
*/

func (h *ClienteHandler) GetAll(ctx iris.Context) {
	cli, err := h.cliente.GetAll()
	if err != nil {
		ctx.StatusCode(404)
		ctx.JSON(context.Map{"response": "No existen registros"})
	}else {
		ctx.StatusCode(200)
		ctx.JSON(context.Map{"response": cli})
	}
}

func (h *ClienteHandler) GetById(ctx iris.Context) {
	id, _ := ctx.Params().GetInt("id")
	cli, err := h.cliente.GetById(id)
	if err != nil {
		ctx.StatusCode(404)
		ctx.JSON(context.Map{"response": "No existe registro"})
	}else {
		ctx.StatusCode(200)
		ctx.JSON(context.Map{"response": cli})
	}
}

func (h *ClienteHandler) GetByNombre(ctx iris.Context) {
	nombre := ctx.Params().Get("nombre")
	cli, err := h.cliente.GetByName(nombre)
	if err != nil {
		ctx.StatusCode(404)
		ctx.JSON(context.Map{"response": "No existe registro"})
	}else {
		ctx.StatusCode(200)
		ctx.JSON(context.Map{"response": cli})
	}
}

func (h *ClienteHandler) GetByCodigo(ctx iris.Context) {
	codigo := ctx.Params().Get("codigo")
	cli, err := h.cliente.GetByCodigo(codigo)
	if err != nil {
		ctx.StatusCode(404)
		ctx.JSON(context.Map{"response": "No existe registro"})
	}else {
		ctx.StatusCode(200)
		ctx.JSON(context.Map{"response": cli})
	}
}

func (h *ClienteHandler) GetByCodigoNombre(ctx iris.Context) {
	codigo := ctx.Params().Get("codigo")
	nombre := ctx.Params().Get("nombre")
	cli, err := h.cliente.GetByCodigoyName(codigo,nombre)
	if err != nil {
		ctx.StatusCode(404)
		ctx.JSON(context.Map{"response": "No existe registro"})
	}else {
		ctx.StatusCode(200)
		ctx.JSON(context.Map{"response": cli})
	}
}

func (h *ClienteHandler) AddCliente(ctx iris.Context) {
	params := models.CLI_Cliente{}
	err := ctx.ReadJSON(&params)
	if err != nil {
		ctx.JSON(context.Map{"response": err.Error()})
	} else {
		result := models.CLI_Cliente{}
		result, err := h.cliente.Save(params)
		if err != nil {
			ctx.StatusCode(404)
			ctx.JSON(context.Map{"response": err.Error()})
		} else {
			ctx.StatusCode(200)
			ctx.JSON(context.Map{"message": "Cliente creado Exitosamente", "response": result})
		}
	}
}

func (h *ClienteHandler) UpdateCliente(ctx iris.Context) {
	params := models.CLI_Cliente{}
	err := ctx.ReadJSON(&params)
	if err != nil {
		ctx.JSON(context.Map{"response": err.Error()})
	} else {
		result := models.CLI_Cliente{}
		result, err := h.cliente.UpdateById(params)
		if err != nil {
			ctx.StatusCode(404)
			ctx.JSON(context.Map{"response": err.Error()})
		} else {
			ctx.StatusCode(200)
			ctx.JSON(context.Map{"message": "Cliente actualizado exitosamente", "response": result})
		}
	}
}

func (h *ClienteHandler) UpdateStatusCliente(ctx iris.Context) {
	params := []models.CLI_Cliente{}
	err := ctx.ReadJSON(&params)
	if err != nil {
		ctx.JSON(context.Map{"response": err.Error()})
	} else {
		_,cant := h.cliente.DeleteByUpdate(params)
		if cant > 0 {
			message := fmt.Sprintf("Se eliminaron %d registros exitosamente\n", cant)
			ctx.StatusCode(200)
			ctx.JSON(context.Map{"response": message})
		} else {
			ctx.StatusCode(404)
			ctx.JSON(context.Map{"response": "No se elimino ningun registro"})
		}
	}
}



